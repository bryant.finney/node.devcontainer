#!/bin/sh
# install system packages
sudo apt update && sudo apt install -y gcc g++ zsh jq gnupg2 git docker docker-compose vim neovim \
    exa man-db procps direnv

# install system dependencies for cypress
sudo apt install -y libgtk2.0-0 libgtk-3-0 libgbm-dev libnotify-dev libgconf-2-4 libnss3 libxss1 \
    libasound2 libxtst6 xauth xvfb

# install additional dependencies for serving the build artifacts
sudo apt install -y xsel && yarn global add serve
