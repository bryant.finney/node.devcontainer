# Node Development Container

Track a
[VSCode development container](https://code.visualstudio.com/docs/remote/create-dev-container)
configuration for use with `Node.js` projects.

## Usage

```sh
# add the `.devcontainer` directory to `.gitignore`
echo .devcontainer >>.gitignore

# clone the devcontainer repo into the Node project root
git clone https://gitlab.com/bryant.finney/node.devcontainer.git .devcontainer

# open in vscode and run the devcontainer
code .
```
